/* Unused


-- Bund
SELECT bund.bund_id, bund.bund_name
        from bund bund
        where bund.bund_id = ;

-- Bundesland
SELECT land.land_id, land.land_name, bund.bund_id, bund.bund_name
        from bundesland land
        left join bund bund on land.land_ref_bund = bund.bund_id
        where land.land_id = ;

-- Regierungsbezirk
SELECT reg.regbez_id, reg.regbez_name, land.land_id, land.land_name, bund.bund_id, bund.bund_name
        from regierungsbezirk reg
        left join bundesland land on reg.regbez_ref_land = land.land_id 
        left join bund bund on land.land_ref_bund = bund.bund_id
        where reg.regbez_id = ;

-- Kreis
SELECT kr.kreis_id, kr.kreis_name, reg.regbez_id, reg.regbez_name, land.land_id, land.land_name, bund.bund_id, bund.bund_name
        from kreis kr
        left join regierungsbezirk reg on kr.kreis_ref_regbez = reg.regbez_id 
        left join bundesland land on reg.regbez_ref_land = land.land_id 
        left join bund bund on land.land_ref_bund = bund.bund_id
        where kr.kreis_id = ;

-- Gemeindeverband
SELECT gv.gv_id, gv.gv_name, kr.kreis_id, kr.kreis_name, reg.regbez_id, reg.regbez_name, land.land_id, land.land_name, bund.bund_id, bund.bund_name 
    from gemeindeverband gv 
    left join kreis kr on gv.gv_ref_kreis = kr.kreis_id 
    left join regierungsbezirk reg on kr.kreis_ref_regbez = reg.regbez_id 
    left join bundesland land on reg.regbez_ref_land = land.land_id 
    left join bund bund on land.land_ref_bund = bund.bund_id
    where gv.gv_id = ;

-- Gemeinde
SELECT gem.gem_id, gem.gem_name, gv.gv_id, gv.gv_name, kr.kreis_id, kr.kreis_name, reg.regbez_id, reg.regbez_name, land.land_id, land.land_name, bund.bund_id, bund.bund_name
        from gemeinde gem
    left join gemeindeverband gv on gem.gem_ref_gv = gv.gv_id
    left join kreis kr on gv.gv_ref_kreis = kr.kreis_id 
    left join regierungsbezirk reg on kr.kreis_ref_regbez = reg.regbez_id 
    left join bundesland land on reg.regbez_ref_land = land.land_id 
    left join bund bund on land.land_ref_bund = bund.bund_id
    where gem.gem_id = ;

-- Everything without names
-- Bund
SELECT bund.bund_id
        from bund bund
        where bund.bund_id = ;

-- Bundesland
SELECT land.land_id, bund.bund_id
        from bundesland land
        left join bund bund on land.land_ref_bund = bund.bund_id
        where land.land_id = ;

-- Regierungsbezirk
SELECT reg.regbez_id, land.land_id, bund.bund_id
        from regierungsbezirk reg
        left join bundesland land on reg.regbez_ref_land = land.land_id 
        left join bund bund on land.land_ref_bund = bund.bund_id
        where reg.regbez_id = ;

-- Kreis
SELECT kr.kreis_id, reg.regbez_id, land.land_id, bund.bund_id
        from kreis kr
        left join regierungsbezirk reg on kr.kreis_ref_regbez = reg.regbez_id 
        left join bundesland land on reg.regbez_ref_land = land.land_id 
        left join bund bund on land.land_ref_bund = bund.bund_id
        where kr.kreis_id = ;

-- Gemeindeverband
SELECT gv.gv_id, kr.kreis_id, reg.regbez_id, land.land_id, bund.bund_id
    from gemeindeverband gv 
    left join kreis kr on gv.gv_ref_kreis = kr.kreis_id 
    left join regierungsbezirk reg on kr.kreis_ref_regbez = reg.regbez_id 
    left join bundesland land on reg.regbez_ref_land = land.land_id 
    left join bund bund on land.land_ref_bund = bund.bund_id
    where gv.gv_id = ;

-- Gemeinde
SELECT gem.gem_id, gv.gv_id, kr.kreis_id, reg.regbez_id, land.land_id, bund.bund_id
        from gemeinde gem
    left join gemeindeverband gv on gem.gem_ref_gv = gv.gv_id
    left join kreis kr on gv.gv_ref_kreis = kr.kreis_id 
    left join regierungsbezirk reg on kr.kreis_ref_regbez = reg.regbez_id 
    left join bundesland land on reg.regbez_ref_land = land.land_id 
    left join bund bund on land.land_ref_bund = bund.bund_id
    where gem.gem_id = ;

*/

-- Other queries
-- Select all measures associated with scope ids.
SELECT me_gb.gb_rel_id, me.me_id, me.me_name, me.me_file_reference, me.me_authority, me.me_date_decree, me.me_date_begin, me.me_date_end, me.me_legal_status, me.me_explanation 
    from measure_geltungsbereich_rel me_gb
    left join measure me on me_gb.me_rel_id = me.me_id
    where me_gb.gb_rel_id in ();

-- Select measure by id.
SELECT me.me_id, me.me_name, me.me_file_reference, me_authority, me_date_decree, me_date_begin, me_date_end, me_legal_status, me_explanation
    from measure me
    where me.me_id = ;

-- Select sources associated with measure ids.
SELECT me.me_id, src.src_id, src.src_link
    from measure me
    left join source src on me.me_id = src.src_ref_me
    where me.me_id in ();

-- Select scope with type by id.
SELECT * from (
    SELECT bund_id id, bund_name name, 6 type from bund
    UNION SELECT land_id id, land_name name, 5 type from bundesland
    UNION SELECT regbez_id id, regbez_name name, 4 type from regierungsbezirk
    UNION SELECT kreis_id id, kreis_name name, 3 type from kreis
    UNION SELECT gv_id id, gv_name name, 2 type from gemeindeverband
    UNION SELECT gem_id id, gem_name name, 1 type from gemeinde
) scopes where scopes.id = ;

-- Select relations between measures. Same stuff in both brackets.
SELECT me_me_id, me_rel_from, me_rel_to, me_rel_effect
    from measure_measure_rel 
    where me_rel_from in () and me_rel_to in ();

-- Select tags for measures.
SELECT me_rel_id as me_id, tag_text
    from measure_tag_rel
    join tag on tag_rel_id = tag_id
    where me_rel_id in ();

-- Select scope with type by measure id.
SELECT mgr.me_rel_id as me_id, scopes.id, scopes.name, scopes.type
    from (
        SELECT bund_id id, bund_name name, 6 type from bund
        UNION SELECT land_id id, land_name name, 5 type from bundesland
        UNION SELECT regbez_id id, regbez_name name, 4 type from regierungsbezirk
        UNION SELECT kreis_id id, kreis_name name, 3 type from kreis
        UNION SELECT gv_id id, gv_name name, 2 type from gemeindeverband
        UNION SELECT gem_id id, gem_name name, 1 type from gemeinde
    ) scopes
    join measure_geltungsbereich_rel mgr on mgr.gb_rel_id = scopes.id
    where mgr.me_rel_id in ();
