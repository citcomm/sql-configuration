DROP DATABASE IF EXISTS cores;
CREATE DATABASE cores;
USE cores;
SOURCE create_tables.sql;
SOURCE create_procedures.sql; 
SOURCE insert_geltungsbereiche.sql;
SOURCE insert_tags.sql;
