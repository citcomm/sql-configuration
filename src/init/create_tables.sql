CREATE TABLE measure (
	me_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
	me_name TEXT NOT NULL,
	me_file_reference TEXT,
	me_authority TEXT NOT NULL,
	me_date_decree DATE NOT NULL,
	me_date_begin DATE NOT NULL,
	me_date_end DATE NOT NULL,
	me_legal_status ENUM('general_ruling', 'regulation', 'administrative_instruction') NOT NULL,
	me_explanation TEXT
) ENGINE=InnoDB;

CREATE TABLE source (
	src_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	src_link TEXT NOT NULL,
	src_ref_me INT NOT NULL,
	FOREIGN KEY (src_ref_me) REFERENCES measure (me_id)
) ENGINE=InnoDB;

CREATE TABLE tag (
	tag_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	tag_text TEXT NOT NULL
) ENGINE = InnoDB;

CREATE TABLE measure_tag_rel (
	me_tag_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	me_rel_id INT NOT NULL,
	tag_rel_id INT NOT NULL,
	FOREIGN KEY (me_rel_id) REFERENCES measure (me_id),
	FOREIGN KEY (tag_rel_id) REFERENCES tag (tag_id)
) ENGINE=InnoDB;

CREATE TABLE measure_measure_rel (
	me_me_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	me_rel_from INT NOT NULL,
	me_rel_to INT NOT NULL,
	me_rel_effect ENUM('suspends', 'extends', 'implements', 'changes', 'overlays', 'other'),
	UNIQUE KEY me_key_relation (me_rel_from, me_rel_to),
	FOREIGN KEY (me_rel_from) REFERENCES measure (me_id),
	FOREIGN KEY (me_rel_to) REFERENCES measure (me_id)
) ENGINE=InnoDB;


CREATE TABLE geltungsbereich (
	gb_id INT NOT NULL PRIMARY KEY
) ENGINE=InnoDB;

CREATE TABLE bund (
	bund_id INT NOT NULL PRIMARY KEY,
	bund_name TEXT NOT NULL,
	FOREIGN KEY (bund_id) REFERENCES geltungsbereich (gb_id)
) ENGINE=InnoDB;

CREATE TABLE bundesland (
	land_id INT NOT NULL PRIMARY KEY,
	land_name TEXT NOT NULL,
	land_ars CHAR(2) NOT NULL,
	land_ref_bund INT NOT NULL,
	FOREIGN KEY (land_id) REFERENCES geltungsbereich (gb_id),
	FOREIGN KEY (land_ref_bund) REFERENCES bund (bund_id)
) ENGINE=InnoDB;

CREATE TABLE regierungsbezirk (
	regbez_id INT NOT NULL PRIMARY KEY,
	regbez_name TEXT NOT NULL,
	regbez_ars CHAR(1) NOT NULL,
	regbez_ref_land INT NOT NULL,
	FOREIGN KEY (regbez_id) REFERENCES geltungsbereich (gb_id),
	FOREIGN KEY (regbez_ref_land) REFERENCES bundesland (land_id)
) ENGINE=InnoDB;

CREATE TABLE kreis (
	kreis_id INT NOT NULL PRIMARY KEY,
	kreis_name TEXT NOT NULL,
	kreis_ars CHAR(2) NOT NULL,
	kreis_ref_regbez INT NOT NULL,
	FOREIGN KEY (kreis_id) REFERENCES geltungsbereich (gb_id),
	FOREIGN KEY (kreis_ref_regbez) REFERENCES regierungsbezirk (regbez_id)
) ENGINE=InnoDB;

CREATE TABLE gemeindeverband (
	gv_id INT NOT NULL PRIMARY KEY,
	gv_name TEXT NOT NULL,
	gv_ars CHAR(4) NOT NULL,
	gv_ref_kreis INT NOT NULL,
	FOREIGN KEY (gv_id) REFERENCES geltungsbereich (gb_id),
	FOREIGN KEY (gv_ref_kreis) REFERENCES kreis (kreis_id)
) ENGINE=InnoDB;

CREATE TABLE gemeinde (
	gem_id INT NOT NULL PRIMARY KEY,
	gem_name TEXT NOT NULL,
	gem_ars CHAR(3) NOT NULL,
	gem_ref_gv INT NOT NULL,
	FOREIGN KEY (gem_id) REFERENCES geltungsbereich (gb_id),
	FOREIGN KEY (gem_ref_gv) REFERENCES gemeindeverband (gv_id)
) ENGINE=InnoDB;


CREATE TABLE measure_geltungsbereich_rel (
	me_gb_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	me_rel_id INT NOT NULL,
	gb_rel_id INT NOT NULL,
	FOREIGN KEY (me_rel_id) REFERENCES measure (me_id),
	FOREIGN KEY (gb_rel_id) REFERENCES geltungsbereich (gb_id)
) ENGINE=InnoDB;
