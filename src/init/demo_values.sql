SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE `measure`;
TRUNCATE `measure_measure_rel`;
TRUNCATE `measure_geltungsbereich_rel`;
TRUNCATE `source`;
TRUNCATE `measure_tag_rel`;
SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO `measure` (`me_name`, `me_authority`, `me_date_decree`, `me_date_begin`, `me_date_end`, `me_legal_status`, `me_explanation`) VALUES ('Allgemeinverfügung der Kreisverwaltung Südliche Weinstraße', 'KV Südliche Weinstraße', '2020-03-20', '2020-03-21', '2020-04-03', 'general_ruling', 'Begründung unter Pdf/Link');
INSERT INTO `measure` (`me_name`, `me_authority`, `me_date_decree`, `me_date_begin`, `me_date_end`) VALUES ('Allgemeinverfügung der Kreisverwaltung Südliche Weinstraße', 'KV Südliche Weinstraße', '2020-03-19', '2020-03-19', '2020-04-19');
INSERT INTO `measure` (`me_name`, `me_authority`, `me_date_decree`, `me_date_begin`, `me_date_end`, `me_legal_status`, `me_explanation`) VALUES ('Erste Coronabekämpfungsverordnung Rheinland-Pfalz', 'Land RLP', '2020-03-19', '2020-03-20', '2020-04-19', 'regulation', 'Begründung unter Pdf/Link');
INSERT INTO `measure` (`me_name`, `me_authority`, `me_date_decree`, `me_date_begin`, `me_date_end`, `me_legal_status`, `me_explanation`) VALUES ('Zweite Coronabekämpfungsverordnung Rheinland-Pfalz', 'Land RLP', '2020-03-19', '2020-03-20', '2020-04-19', 'administrative_instruction', 'Begründung unter Pdf/Link');
INSERT INTO `measure` (`me_name`, `me_file_reference`, `me_authority`, `me_date_decree`, `me_date_begin`, `me_date_end`, `me_legal_status`, `me_explanation`) VALUES ('Vollzug des Infektionsschutzgesetzes (lfSG) – Maßnahmen anlässlich der Corona-Pandemie', 'Az- 093/1-21', 'Landkreis Tischenreuth', '2020-03-18', '2020-03-20', '2020-04-02', 'general_ruling', 'Begründung im pdf ');
INSERT INTO `measure` (`me_name`, `me_file_reference`, `me_authority`, `me_date_decree`, `me_date_begin`, `me_date_end`, `me_legal_status`, `me_explanation`) VALUES ("Maßnahmen zur Coronabekämpfung", NULL, "BRD", 2020-03-31, 2020-04-01, 2020-12-31, 'regulation', "Das Virus ist gefährlich.");

INSERT INTO `measure_measure_rel` (`me_rel_from`, `me_rel_to`, `me_rel_effect`) VALUES ('4', '3', 'extends');
INSERT INTO `measure_measure_rel` (`me_rel_from`, `me_rel_to`, `me_rel_effect`) VALUES ('1', '2', 'overlays');

INSERT INTO `measure_geltungsbereich_rel` (`me_rel_id`, `gb_rel_id`) VALUES ('3', '8');
INSERT INTO `measure_geltungsbereich_rel` (`me_rel_id`, `gb_rel_id`) VALUES ('4', '8');
INSERT INTO `measure_geltungsbereich_rel` (`me_rel_id`, `gb_rel_id`) VALUES ('1', '205');
INSERT INTO `measure_geltungsbereich_rel` (`me_rel_id`, `gb_rel_id`) VALUES ('2', '205');
INSERT INTO `measure_geltungsbereich_rel` (`me_rel_id`, `gb_rel_id`) VALUES ('5', '13557');
INSERT INTO `measure_geltungsbereich_rel` (`me_rel_id`, `gb_rel_id`) VALUES (6, 1);

INSERT INTO `source` (`src_link`, `src_ref_me`) VALUES ('https://www.suedliche-weinstrasse.de/media/docs/aktuelles/amtsblatt/2020/amtsblatt18_2020.pdf', '1');
INSERT INTO `source` (`src_link`, `src_ref_me`) VALUES ('https://www.suedliche-weinstrasse.de/media/docs/aktuelles/amtsblatt/2020/amtsblatt16_2020.pdf', '2');
INSERT INTO `source` (`src_link`, `src_ref_me`) VALUES ('https://www.rlp.de/fileadmin/rlp-stk/pdf-Dateien/Anlagen_fuer_Pressemitteilungen/GVBl_Nr._05_vom_20.03.2020.pdf', '3');
INSERT INTO `source` (`src_link`, `src_ref_me`) VALUES ('https://www.rlp.de/fileadmin/rlp-stk/pdf-Dateien/Anlagen_fuer_Pressemitteilungen/2020-03-20_2._CoBeLVO.pdf', '4');
INSERT INTO `source` (`src_link`, `src_ref_me`) VALUES ('https://www.kreis-tir.de/fileadmin/user_upload/Kreisorgane/Amtsblatt/Amtsblatt_2020/Amtsblatt_12_a.pdf', '5');

INSERT INTO `measure_tag_rel` (`me_rel_id`, `tag_rel_id`) VALUES ('1', '1');
INSERT INTO `measure_tag_rel` (`me_rel_id`, `tag_rel_id`) VALUES ('1', '17');
INSERT INTO `measure_tag_rel` (`me_rel_id`, `tag_rel_id`) VALUES ('5', '23');


