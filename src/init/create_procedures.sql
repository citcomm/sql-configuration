DROP PROCEDURE IF EXISTS searchSuggestion;
DROP PROCEDURE IF EXISTS getParentScopes;
DELIMITER //
CREATE PROCEDURE searchSuggestion (searchString TEXT, maxEntries INT)
BEGIN
SELECT coalesce(t.gem_id, t.gv_id, t.kreis_id, t.regbez_id, t.land_id, t.bund_id) id, t.bund_name, t.land_name, t.regbez_name, t.kreis_name, t.gv_name, t.gem_name
    from (
        SELECT bund.bund_id, bund.bund_name, landtemp.land_id, landtemp.land_name, landtemp.regbez_id, landtemp.regbez_name, landtemp.kreis_id, landtemp.kreis_name, landtemp.gv_id, landtemp.gv_name, landtemp.gem_id, landtemp.gem_name
            from bund 
            right join (
                SELECT land.land_id, land.land_name, land.land_ref_bund, regbeztemp.regbez_id, regbeztemp.regbez_name, regbeztemp.kreis_id, regbeztemp.kreis_name, regbeztemp.gv_id, regbeztemp.gv_name, regbeztemp.gem_id, regbeztemp.gem_name
                    from bundesland land
                    right join (
                        SELECT regbez.regbez_id, regbez.regbez_name, regbez.regbez_ref_land, kreistemp.kreis_id, kreistemp.kreis_name, kreistemp.gv_id, kreistemp.gv_name, kreistemp.gem_id, kreistemp.gem_name
                            from regierungsbezirk regbez
                            right join (
                                SELECT kreis.kreis_id, kreis.kreis_name, kreis.kreis_ref_regbez, gvtemp.gv_id, gvtemp.gv_name, gvtemp.gem_id, gvtemp.gem_name
                                    from kreis kreis
                                    right join (
                                        SELECT gv.gv_id, gv.gv_name, gv.gv_ref_kreis, gemtemp.gem_id, gemtemp.gem_name
                                            from gemeindeverband gv
                                            right join (
                                                SELECT gem.gem_id, gem.gem_name, gem.gem_ref_gv
                                                from gemeinde gem
                                                where gem.gem_name LIKE searchString
                                            ) gemtemp on gemtemp.gem_ref_gv = gv.gv_id
                                        UNION SELECT gv.gv_id, gv.gv_name, gv.gv_ref_kreis, NULL as gem_id, NULL as gem_name from gemeindeverband gv where gv.gv_name LIKE searchString
                                    ) gvtemp on gvtemp.gv_ref_kreis = kreis.kreis_id
                                UNION SELECT kreis.kreis_id, kreis.kreis_name, kreis.kreis_ref_regbez, NULL as gv_id, NULL as gv_name, NULL as gem_id, NULL as gem_name from kreis kreis where kreis.kreis_name LIKE searchString
                            ) kreistemp on kreistemp.kreis_ref_regbez = regbez.regbez_id
                        UNION SELECT regbez.regbez_id, regbez.regbez_name, regbez.regbez_ref_land, NULL as kreis_id, NULL as kreis_name, NULL as gv_id, NULL as gv_name, NULL as gem_id, NULL as gem_name from regierungsbezirk regbez where regbez.regbez_name LIKE searchString
                    ) regbeztemp on regbeztemp.regbez_ref_land = land.land_id
                UNION SELECT land.land_id, land.land_name, land.land_ref_bund, NULL as regbez_id, NULL as regbez_name, NULL as kreis_id, NULL as kreis_name, NULL as gv_id, NULL as gv_name, NULL as gem_id, NULL as gem_name from bundesland land where land.land_name LIKE searchString
            ) landtemp on landtemp.land_ref_bund = bund.bund_id
        UNION SELECT bund.bund_id, bund.bund_name, NULL as land_id, NULL as land_name, NULL as regbez_id, NULL as regbez_name, NULL as kreis_id, NULL as kreis_name, NULL as gv_id, NULL as gv_name, NULL as gem_id, NULL as gem_name from bund bund where bund.bund_name LIKE searchString
    ) t order by id asc limit maxEntries;
END;
//
DELIMITER ;

DELIMITER //
CREATE PROCEDURE getParentScopes (searchId INT)
BEGIN
SELECT t.bund_id, t.land_id, t.regbez_id, t.kreis_id, t.gv_id, t.gem_id
    from (
        SELECT bund.bund_id, bund.bund_name, landtemp.land_id, landtemp.land_name, landtemp.regbez_id, landtemp.regbez_name, landtemp.kreis_id, landtemp.kreis_name, landtemp.gv_id, landtemp.gv_name, landtemp.gem_id, landtemp.gem_name
            from bund 
            right join (
                SELECT land.land_id, land.land_name, land.land_ref_bund, regbeztemp.regbez_id, regbeztemp.regbez_name, regbeztemp.kreis_id, regbeztemp.kreis_name, regbeztemp.gv_id, regbeztemp.gv_name, regbeztemp.gem_id, regbeztemp.gem_name
                    from bundesland land
                    right join (
                        SELECT regbez.regbez_id, regbez.regbez_name, regbez.regbez_ref_land, kreistemp.kreis_id, kreistemp.kreis_name, kreistemp.gv_id, kreistemp.gv_name, kreistemp.gem_id, kreistemp.gem_name
                            from regierungsbezirk regbez
                            right join (
                                SELECT kreis.kreis_id, kreis.kreis_name, kreis.kreis_ref_regbez, gvtemp.gv_id, gvtemp.gv_name, gvtemp.gem_id, gvtemp.gem_name
                                    from kreis kreis
                                    right join (
                                        SELECT gv.gv_id, gv.gv_name, gv.gv_ref_kreis, gemtemp.gem_id, gemtemp.gem_name
                                            from gemeindeverband gv
                                            right join (
                                                SELECT gem.gem_id, gem.gem_name, gem.gem_ref_gv
                                                from gemeinde gem
                                                where gem.gem_id = searchId
                                            ) gemtemp on gemtemp.gem_ref_gv = gv.gv_id
                                        UNION SELECT gv.gv_id, gv.gv_name, gv.gv_ref_kreis, NULL as gem_id, NULL as gem_name from gemeindeverband gv where gv.gv_id = searchId
                                    ) gvtemp on gvtemp.gv_ref_kreis = kreis.kreis_id
                                UNION SELECT kreis.kreis_id, kreis.kreis_name, kreis.kreis_ref_regbez, NULL as gv_id, NULL as gv_name, NULL as gem_id, NULL as gem_name from kreis kreis where kreis.kreis_id = searchId
                            ) kreistemp on kreistemp.kreis_ref_regbez = regbez.regbez_id
                        UNION SELECT regbez.regbez_id, regbez.regbez_name, regbez.regbez_ref_land, NULL as kreis_id, NULL as kreis_name, NULL as gv_id, NULL as gv_name, NULL as gem_id, NULL as gem_name from regierungsbezirk regbez where regbez.regbez_id = searchId
                    ) regbeztemp on regbeztemp.regbez_ref_land = land.land_id
                UNION SELECT land.land_id, land.land_name, land.land_ref_bund, NULL as regbez_id, NULL as regbez_name, NULL as kreis_id, NULL as kreis_name, NULL as gv_id, NULL as gv_name, NULL as gem_id, NULL as gem_name from bundesland land where land.land_id = searchId
            ) landtemp on landtemp.land_ref_bund = bund.bund_id
        UNION SELECT bund.bund_id, bund.bund_name, NULL as land_id, NULL as land_name, NULL as regbez_id, NULL as regbez_name, NULL as kreis_id, NULL as kreis_name, NULL as gv_id, NULL as gv_name, NULL as gem_id, NULL as gem_name from bund bund where bund.bund_id = searchId
    ) t;
END;
//
DELIMITER ;
