# Datenbank zum CoReS-Projekt
In diesem Repository liegen die Dateien zur Konfiguration der zum CoReS-Projekt gehörenden Datenbank sowie einige Dateien mit hilfreichen SQL-Statements.

## Einrichtung
Zum Einrichten einer Testing-Instanz einfach mit dem Root-User das Script `init_testing_server.sql` im Ordner `src/init` ausführen.
Wenn nur die Datenban aufgesetzt werden soll und nicht zusätzlich noch der User konfiguriert werden soll, kann alternativ auch die `init_testing_database.sql` aufgeführt werden.
Wenn die Datenbankinstanz nicht zum Testing gedacht ist, gibt es beide Scripts auch ohne den Zusatz `testing_`.

### License

Dieses Projekt ist lizensiert unter der European Union Public Licence (EUPL) Version 1.2 oder neuer (ABl. EU L 128, 17. Mai 2017, S. 59; eupl.eu). Gemäß dieser Lizenz wird keine Gewährleistung gewährt und ein Haftungsausschluss (außer für Vorsatz und Personenschäden) vereinbart.

This project is licensed under the European Union Public Licence (EUPL) version 1.2 or later (OJEU, L 128, 19 May 2017, p. 59; eupl.eu).
According to this license it comes without warranties and the Licensor will in no event be liable, except in the cases of wilful misconduct or damages directly caused to natural persons.
